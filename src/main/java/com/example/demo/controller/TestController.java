package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

  @RequestMapping("/")
  public double home() {
    double sequenceFormula = 0;
    for(int counter = 1; counter < 100000000; counter += 2) {
      sequenceFormula = sequenceFormula + ((1 / (2 * counter - 1)) - (1 / (2 * counter + 1)));
    }
    double pi = 4 * sequenceFormula;
    return pi;
  }
}